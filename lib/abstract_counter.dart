abstract class AbstractCounter{
  void increment();

  void decrement();  

  void resetCount();

  int getCount();

  String getName();
}