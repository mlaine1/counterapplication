import 'package:flutter/material.dart';

import './counter_card.dart';
import './counter_list.dart';

class CounterPage extends StatefulWidget {
  @override
  CounterPageState createState() => new CounterPageState();
}

class CounterPageState extends State<CounterPage> {
  CounterList counters = new CounterList();
  final TextEditingController textController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Counters"),
      ),
      body: new ListView.builder(
        itemCount: counters.length(),
        itemBuilder: (context, index) {
          final counterName = counters.getNameFromIndex(index);
          return Dismissible(
            dismissThresholds: const {DismissDirection.endToStart: 0.5, DismissDirection.startToEnd: 0.5},    
            key: ValueKey(counterName),
            child: new CounterCard(counterName, counters),
            onDismissed: (direction) {
              setState(() {
                counters.removeCounter(counterName);
              });
              Scaffold.of(context).showSnackBar(
                  SnackBar(content: Text("$counterName Dismissed")));
            },
            background: Container(
                color: Colors.red,
                alignment: Alignment.centerLeft,
                child: new Icon(
                  Icons.delete_forever,
                  color: Colors.white,
                  size: 50,
                )),
            secondaryBackground: Container(
              color: Colors.red,
                alignment: Alignment.centerRight,
                child: new Icon(
                  Icons.delete_forever,
                  color: Colors.white,
                  size: 50,
                )
            ),
          );
        },
      ),
      floatingActionButton: new FloatingActionButton(
        onPressed: _showDialog,
        child: new Icon(Icons.add),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      resizeToAvoidBottomPadding: false,
    );
  }

  _showDialog() async {
    await showDialog<String>(
        context: context,
        child: new SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: new _SystemPadding(
            child: new AlertDialog(
              contentPadding: const EdgeInsets.all(16.0),
              content: new Row(
                children: <Widget>[
                  new Expanded(
                    child: new TextField(
                      autofocus: true,
                      decoration: new InputDecoration(
                          labelText: 'Counter Name',
                          hintText: 'eg. number of pencils'),
                      onSubmitted: (String str) {
                        setState(() {
                          counters.addCounter(str);
                          Navigator.pop(context);
                        });
                        textController.text = "";
                      },
                      controller: textController,
                    ),
                  )
                ],
              ),
              actions: <Widget>[
                new FlatButton(
                    child: const Text('CANCEL'),
                    onPressed: () {
                      textController.text = "";
                      Navigator.pop(context);
                    }),
                new FlatButton(
                    child: const Text('OK'),
                    onPressed: () {
                      setState(() {
                        counters.addCounter(textController.text);
                        Navigator.pop(context);
                      });
                      textController.text = "";
                    })
              ],
            ),
          ),
        ));
  }
}

class _SystemPadding extends StatelessWidget {
  final Widget child;

  _SystemPadding({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    return new AnimatedContainer(
        padding: mediaQuery.viewInsets,
        duration: const Duration(milliseconds: 300),
        child: child);
  }
}
