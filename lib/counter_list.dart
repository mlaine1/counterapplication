import 'package:counter/counter.dart';
import 'package:counter/abstract_counter_list.dart';

class CounterList implements AbstractCounterList {
  List<Counter> counterList;

  CounterList() {
    this.counterList = new List<Counter>();
  }

  @override
  void addCounter(String counterName) {
    if (!hasCounter(counterName)) {
      this.counterList.add(new Counter(counterName));
    }
  }

  @override
  Counter findCounter(String counterName) {
    if (hasCounter(counterName)) {
      int indexOfCounter = getIndexOfCounter(counterName);
      return this.counterList[indexOfCounter];
    } else {
      return null;
    }
  }

  @override
  void removeCounter(String counterName) {
    if (hasCounter(counterName)) {
      int indexOfCounter = getIndexOfCounter(counterName);
      this.counterList.removeAt(indexOfCounter);
    }
  }

   @override
  void resetCounter(String counterName) {
    int indexOfCounter = getIndexOfCounter(counterName);
    this.counterList[indexOfCounter].resetCount();
  }  

  @override
  String getNameFromIndex(int index) {
    return this.counterList[index].getName();
  }

  @override
  bool hasCounter(String counterName) {
    if (getIndexOfCounter(counterName) == -1) {
      return false;
    }
    return true;
  }

  @override
  int length() {
    return this.counterList.length;
  }

  @override
  Counter removeAny() {
    if (length() > 0) {
      return this.counterList.removeAt(0);
    } else {
      return null;
    }
  }

  int getIndexOfCounter(String counterName) {
    int indexOfCounter = -1;
    for (int i = 0; i < this.counterList.length; i++) {
      if (this.counterList[i].getName() == counterName) {
        indexOfCounter = i;
        break;
      }
    }
    return indexOfCounter;
  } 
}
