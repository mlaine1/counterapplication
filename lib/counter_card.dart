import 'package:counter/counter_list.dart';
import 'package:flutter/material.dart';

class CounterCard extends StatefulWidget {
  String _counterName1;
  CounterList _counterList;
  CounterCard(String counterName, CounterList counters) {
    _counterName1 = counterName;
    _counterList = counters;
  }

  @override
  State<StatefulWidget> createState() =>
      new CounterCardState(_counterName1, _counterList);
}

class CounterCardState extends State<CounterCard> {
  String _counterName;
  CounterList _counters;
  String toDisplay;

  CounterCardState(counterName, counters) {
    _counterName = counterName;
    _counters = counters;
    toDisplay = _counterName + "\nCount: " + "0";
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: InkWell(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              ListTile(                
                leading: Icon(Icons.iso),
                title: Text(toDisplay),
              ),
              ButtonTheme.bar(
                child: ButtonBar(
                  children: <Widget>[
                    IconButton(
                      padding: const EdgeInsets.all(2),                       
                      icon: new Icon(Icons.restore),
                      iconSize: 35, 
                      color: Colors.blueAccent.shade200,
                      onPressed: (){
                        setState(() {
                          _counters.findCounter(_counterName).resetCount();
                          toDisplay = _counterName +
                              "\nCount: " +
                              _counters
                                  .findCounter(_counterName)
                                  .getCount()
                                  .toString();
                        });
                      },
                    ),
                    IconButton(
                      padding: const EdgeInsets.all(2),                       
                      icon: new Icon(Icons.remove_circle),
                      iconSize: 35, 
                      color: Colors.redAccent.shade200,
                      onPressed: (){
                        setState(() {
                          _counters.findCounter(_counterName).decrement();
                          toDisplay = _counterName +
                              "\nCount: " +
                              _counters
                                  .findCounter(_counterName)
                                  .getCount()
                                  .toString();
                        });
                      },
                    ),
                    IconButton(
                      padding: const EdgeInsets.all(2),                      
                      icon: new Icon(Icons.add_circle),
                      iconSize: 35, 
                      color: Colors.greenAccent.shade200,
                      onPressed: (){
                        setState(() {
                          _counters.findCounter(_counterName).increment();
                          toDisplay = _counterName +
                              "\nCount: " +
                              _counters
                                  .findCounter(_counterName)
                                  .getCount()
                                  .toString();
                        });
                      },
                    ),

                    /*FlatButton(
                      child: const Text('Decrement'),
                      onPressed: () {
                        setState(() {
                          _counters.findCounter(_counterName).decrement();
                          toDisplay = _counterName +
                              "\nCount: " +
                              _counters
                                  .findCounter(_counterName)
                                  .getCount()
                                  .toString();
                        });
                      },
                    ),*/
                    /*FlatButton(
                      child: const Text('Increment'),
                      onPressed: () {
                        setState(() {
                          _counters.findCounter(_counterName).increment();
                          toDisplay = _counterName +
                              "\nCount: " +
                              _counters
                                  .findCounter(_counterName)
                                  .getCount()
                                  .toString();
                        });
                      },
                    ),*/
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
