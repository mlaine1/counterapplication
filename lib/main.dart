import 'package:flutter/material.dart';

import './counter_page.dart';

void main() {
  runApp(MyApp());//new MaterialApp(home: new CounterPage()));
}

class MyApp extends StatefulWidget{
  @override
  MyAppState createState() => new MyAppState();
}


class MyAppState extends State<MyApp>{
  @override
  Widget build (BuildContext context){
    return new MaterialApp(
      home: new CounterPage()
    );
  }
}