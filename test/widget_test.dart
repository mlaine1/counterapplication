// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:counter/main.dart';

void main() {
  testWidgets('Adding, incrementing, and removing one widget', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(MyApp());
    await tester.pumpAndSettle();

    //click the add button
    await tester.tap(find.byIcon(Icons.add));
    await tester.pumpAndSettle();

    //enter "Test" into the popup textfield
    await tester.enterText(find.byType(TextField), "Test");
    await tester.pumpAndSettle(); 

    //press OK to create new widget
    await tester.tap(find.text('OK'));
    await tester.pumpAndSettle();

    //check that the counter was made
    expect(find.text('Test\nCount: 0'), findsOneWidget);

    //press the increment button
    await tester.tap(find.text('Increment'));
    await tester.pumpAndSettle();

    //check that it was incremented
    expect(find.text('Test\nCount: 1'), findsOneWidget);
    expect(find.text('Test\nCount: 0'), findsNothing);

    //swipe the counter off the screen
    await tester.drag(find.byType(Card), const Offset(-500, 0));
    await tester.pumpAndSettle();

    //check that it was removed
    expect(find.text('Test\nCount: 1'), findsNothing);
    expect(find.text('Test\nCount: 0'), findsNothing);
  });

  testWidgets('Adding 2 widgets, increment the first and remove', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(MyApp());
    await tester.pumpAndSettle();

    //click the add button
    await tester.tap(find.byIcon(Icons.add));
    await tester.pumpAndSettle();

    //enter "Test1" into the popup textfield
    await tester.enterText(find.byType(TextField), "Test1");
    await tester.pumpAndSettle(); 

    //press OK to create new widget
    await tester.tap(find.text('OK'));
    await tester.pumpAndSettle();

    //check that the counter was made
    expect(find.text('Test1\nCount: 0'), findsOneWidget);
    expect(find.text('Test2\nCount: 0'), findsNothing);

    //press the increment button
    await tester.tap(find.text('Increment'));
    await tester.pumpAndSettle();

    //check that it was incremented
    expect(find.text('Test1\nCount: 1'), findsOneWidget);
    expect(find.text('Test1\nCount: 0'), findsNothing);
    expect(find.text('Test2\nCount: 0'), findsNothing);

    //click the add button
    await tester.tap(find.byIcon(Icons.add));
    await tester.pumpAndSettle();

    //enter "Test2" into the popup textfield
    await tester.enterText(find.byType(TextField), "Test2");
    await tester.pumpAndSettle(); 

    //press OK to create new counter
    await tester.tap(find.text('OK'));
    await tester.pumpAndSettle();

    //check that the counter was made
    expect(find.text('Test1\nCount: 1'), findsOneWidget);
    expect(find.text('Test1\nCount: 0'), findsNothing);
    expect(find.text('Test2\nCount: 0'), findsOneWidget); 

    //swipe the counter off the screen
    await tester.drag(find.text("Test1\nCount: 1"), const Offset(-500, 0));
    await tester.pumpAndSettle();      

    //check that the counter was removed
    expect(find.text('Test1\nCount: 1'), findsNothing);
    expect(find.text('Test1\nCount: 0'), findsNothing);
    expect(find.text('Test2\nCount: 0'), findsOneWidget);  
  });

  testWidgets('Adding 3 widgets, increment the first and remove the second', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(MyApp());
    await tester.pumpAndSettle();

    //click the add button
    await tester.tap(find.byIcon(Icons.add));
    await tester.pumpAndSettle();

    //enter "Test1" into the popup textfield
    await tester.enterText(find.byType(TextField), "Test1");
    await tester.pumpAndSettle(); 

    //press OK to create new widget
    await tester.tap(find.text('OK'));
    await tester.pumpAndSettle();

    //check that the counter was made
    expect(find.text('Test1\nCount: 0'), findsOneWidget);
    expect(find.text('Test2\nCount: 0'), findsNothing);

    //press the increment button
    await tester.tap(find.text('Increment'));
    await tester.pumpAndSettle();

    //check that it was incremented
    expect(find.text('Test1\nCount: 1'), findsOneWidget);
    expect(find.text('Test1\nCount: 0'), findsNothing);
    expect(find.text('Test2\nCount: 0'), findsNothing);

    //click the add button
    await tester.tap(find.byIcon(Icons.add));
    await tester.pumpAndSettle();

    //enter "Test2" into the popup textfield
    await tester.enterText(find.byType(TextField), "Test2");
    await tester.pumpAndSettle(); 

    //press OK to create new counter
    await tester.tap(find.text('OK'));
    await tester.pumpAndSettle();

    //check that the counter was made
    expect(find.text('Test1\nCount: 1'), findsOneWidget);
    expect(find.text('Test1\nCount: 0'), findsNothing);
    expect(find.text('Test2\nCount: 0'), findsOneWidget);

    //click the add button
    await tester.tap(find.byIcon(Icons.add));
    await tester.pumpAndSettle();

    //enter "Test3" into the popup textfield
    await tester.enterText(find.byType(TextField), "Test3");
    await tester.pumpAndSettle(); 

    //press OK to create new counter
    await tester.tap(find.text('OK'));
    await tester.pumpAndSettle();

    //check that the counter was made
    expect(find.text('Test1\nCount: 1'), findsOneWidget);
    expect(find.text('Test1\nCount: 0'), findsNothing);
    expect(find.text('Test2\nCount: 0'), findsOneWidget);
    expect(find.text('Test3\nCount: 0'), findsOneWidget);

    //swipe the counter off the screen
    await tester.drag(find.text("Test2\nCount: 0"), const Offset(-500, 0));
    await tester.pumpAndSettle();      

    //check that the counter was removed
    expect(find.text('Test1\nCount: 1'), findsOneWidget);
    expect(find.text('Test1\nCount: 0'), findsNothing);
    expect(find.text('Test2\nCount: 0'), findsNothing);
    expect(find.text('Test3\nCount: 0'), findsOneWidget);  
  });
}
