import 'package:counter/counter_list.dart';
import 'package:counter/counter.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  bool listsEqualClearsBothLists(CounterList list1, CounterList list2){
    if(list1.length() != list2.length()){
      return false;
    }else{
      while(list2.length() > 0){
        Counter tempCounter1 = list2.removeAny();
        if(!list1.hasCounter(tempCounter1.getName())){
           return false; 
        }else{
          Counter tempCounter2 = list1.findCounter(tempCounter1.getName());
          if(tempCounter1.getCount() != tempCounter2.getCount()){
            return false;
          }
        }
      }
    }
    return true;
  }

  test("AddCounter: adds a new counter to the list: to empty List", () {
    String counterName1 = "ExampleName1";
    CounterList actualCounterList = new CounterList();
    CounterList expectedCounterList = new CounterList();
    
    actualCounterList.addCounter(counterName1);
    expectedCounterList.addCounter(counterName1);
    
    expect(actualCounterList.hasCounter(counterName1), expectedCounterList.hasCounter(counterName1));
    expect(listsEqualClearsBothLists(actualCounterList, expectedCounterList), true);
  });

  test("AddCounter: adds a new counter to the list, doesn't add because contains counter", () {
    String counterName1 = "ExampleName1";
    CounterList actualCounterList = new CounterList();
    CounterList expectedCounterList = new CounterList();
    
    actualCounterList.addCounter(counterName1);
    expectedCounterList.addCounter(counterName1);

    actualCounterList.addCounter(counterName1);
    
    expect(actualCounterList.hasCounter(counterName1), expectedCounterList.hasCounter(counterName1));
    expect(listsEqualClearsBothLists(actualCounterList, expectedCounterList), true);
  });
  
  test("RemoveCounter: removes a counter from the list leaving an empty list", () {
    String counterName = "ExampleName1";
    CounterList actualCounterList = new CounterList();
    CounterList expectedCounterList = new CounterList();
    
    actualCounterList.addCounter(counterName);
    actualCounterList.removeCounter(counterName);

    expect(listsEqualClearsBothLists(actualCounterList, expectedCounterList), true);
  });

  test("RemoveCounter: removes a counter from the list leaving a single counter", () {
    String counterName1 = "ExampleName1";
    String counterName2 = "ExampleName2";
    CounterList actualCounterList = new CounterList();
    CounterList expectedCounterList = new CounterList();
    
    actualCounterList.addCounter(counterName1);    
    actualCounterList.addCounter(counterName2);
    actualCounterList.removeCounter(counterName2);    
    expectedCounterList.addCounter(counterName1);

    expect(actualCounterList.hasCounter(counterName1), expectedCounterList.hasCounter(counterName1));
    expect(listsEqualClearsBothLists(actualCounterList, expectedCounterList), true);
  });

  test("ResetCounter: resets a counter from the list to 0", () {
    String counterName1 = "ExampleName1";
    CounterList actualCounterList = new CounterList();
    
    actualCounterList.addCounter(counterName1);    

    actualCounterList.findCounter(counterName1).increment();
    actualCounterList.findCounter(counterName1).increment();
    actualCounterList.findCounter(counterName1).increment();
    int expectedFirstCount = 3;
    int actualFirstCount = actualCounterList.findCounter(counterName1).getCount();
    
    actualCounterList.resetCounter(counterName1);

    int actualFinalCount = actualCounterList.findCounter(counterName1).getCount();
    int expectedFinalCount = 0;
    
    expect(actualFirstCount, expectedFirstCount);
    expect(actualFinalCount, expectedFinalCount);
  });

  test("FindCounter: returns an alias to a counter inside the list", () {
    String counterName1 = "ExampleName1";
    CounterList actualCounterList = new CounterList();
    CounterList expectedCounterList = new CounterList();    

    actualCounterList.addCounter(counterName1);
    expectedCounterList.addCounter(counterName1);
    Counter actualCounter = actualCounterList.findCounter(counterName1);
    Counter expectedCounter = new Counter(counterName1);    
    
    expect(actualCounter.getCount(), expectedCounter.getCount());
    expect(actualCounter.getName(), expectedCounter.getName());
    expect(actualCounterList.hasCounter(counterName1), expectedCounterList.hasCounter(counterName1));
    expect(listsEqualClearsBothLists(actualCounterList, expectedCounterList), true);
  });

  test("GetNameFromIndex: gets the name of the counter at the index: 0", (){
    int indexOfName = 0;
    String counterName1 = "ExampleName1";
    CounterList counterList = new CounterList();

    counterList.addCounter(counterName1);
    String actualName = counterList.getNameFromIndex(indexOfName);
    String expectedName = "ExampleName1";

    expect(actualName, expectedName);
  });

  test("Length: returns the length of the CounterList: 0", () {
    CounterList counterList = new CounterList();

    int actualLength = counterList.length();
    int expectedLength = 0;
    
    expect(actualLength, expectedLength);
  });

  test("Length: returns the length of the CounterList: 1", () {
    String counterName = "ExampleName";
    CounterList counterList = new CounterList();
    counterList.addCounter(counterName);

    int actualLength = counterList.length();
    int expectedLength = 1;
    
    expect(actualLength, expectedLength);
  });

  test("HasCounter: returns true if counter is in CoutnerList: Counter not in empty list", () {
    String nameToFind = "Pencils";
    CounterList counterList = new CounterList();

    bool inCounterList = counterList.hasCounter(nameToFind);

    expect(inCounterList, false);
  });

  test("HasCounter: returns true if counter is in CoutnerList: Counter in non empty list", () {
    String counterName = "Pencils";
    String nameToFind = "Pencils";
    CounterList counterList = new CounterList();
    counterList.addCounter(counterName);

    bool inCounterList = counterList.hasCounter(nameToFind);

    expect(inCounterList, true);
  });

  test("HasCounter: returns true if counter is in CoutnerList: Counter not in non empty list", () {
    String counterName = "Pencils";
    String nameToFind = "Erasers";
    CounterList counterList = new CounterList();
    counterList.addCounter(counterName);

    bool inCounterList = counterList.hasCounter(nameToFind);

    expect(inCounterList, false);
  });       
}
