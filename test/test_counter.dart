import 'package:counter/counter.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test("Increment: increases count by 1 from 0", () {
    Counter counter = new Counter("ExampleName");
    int expectedCount = 1;
    counter.increment();
    expect(counter.getCount(), expectedCount);
  });

  test("Decrement: decreases count by 1 from 0", () {
    Counter actualCount = new Counter("ExampleName");
    int expectedCount = -1;
    actualCount.decrement();
    expect(actualCount.getCount(), expectedCount);
  });

  test("GetCount: returns the value of the count: 0", () {
    Counter counter = new Counter("ExampleName");
    int expectedCount = 0;
    int actualCount = counter.getCount();
    expect(actualCount, expectedCount);
  });

  test("GetName: returns the name of the counter: ExampleName", () {
    Counter counter = new Counter("ExampleName");
    String expectedName = "ExampleName";
    String actualName = counter.getName();
    expect(actualName, expectedName);
  });

  test("ResetCount: sets the value of count to 0 from 0", () {
    Counter counter = new Counter("ExampleName");
    int expectedCount = 0;
    counter.resetCount();
    int actualCount = counter.getCount();
    expect(actualCount, expectedCount);
  });

  test("ResetCount: sets the value of count to 0 from 1", () {
    Counter counter = new Counter("ExampleName");
    int expectedCount = 0;
    counter.increment();
    counter.resetCount();
    int actualCount = counter.getCount();
    expect(actualCount, expectedCount);
  });
}
