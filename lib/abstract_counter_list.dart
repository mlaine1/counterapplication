import 'package:counter/counter.dart';

abstract class AbstractCounterList {
  void addCounter(String counterName);
  
  void removeCounter(String counterName);

  Counter findCounter(String counterName);

  String getNameFromIndex(int index);

  void resetCounter(String counterName);

  Counter removeAny();

  bool hasCounter(String counterName);

  int length();  
}