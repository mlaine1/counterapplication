import 'package:counter/abstract_counter.dart';

class Counter implements AbstractCounter{

  String name;
  int count;

  Counter(String name){
    this.name = name;
    this.count = 0;
  }

  @override
  void increment() {
    count++;
  }

  @override
  void decrement() {
    count--;
  }

  @override
  int getCount() {
    return count;
  }

  @override
  String getName(){
    return this.name;
  }

  @override
  void resetCount(){
    this.count = 0;
  }
}